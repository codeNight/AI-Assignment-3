package model.actions;

import model.state.State;

import java.util.List;

public interface ActionsController {
    /**
     * @returns a list of {@link StochasticAction}s that are legal to take in the current state
     */
    List<StochasticAction> getLegalActions(State currentState);
}
