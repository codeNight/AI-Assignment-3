package model.actions;

import model.state.State;
import model.state.StochasticActionOutput;

import java.util.List;

/**
 * Represents a non-deterministic action where the output of the action could be one of many outputs with probability
 * for each output.
 */
public interface StochasticAction {
    /**
     * accepts a current state and returns all of the possible outcomes of evaluating that output.
     *
     * @param current: is the current state
     * @returns a list of {@link StochasticActionOutput} that are all of the possible
     * outcomes of evaluating that action in the current state.
     *
     * @throws IllegalArgumentException if this action is not legal in the current state
     */
    List<StochasticActionOutput> accept(State current) throws IllegalArgumentException;
}
