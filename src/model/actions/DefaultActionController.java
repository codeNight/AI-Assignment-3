package model.actions;

import model.state.State;

import java.util.Arrays;
import java.util.List;

public class DefaultActionController implements ActionsController {

    @Override
    public List<StochasticAction> getLegalActions(State currentState) {
        return Arrays.asList(Actions.UP, Actions.DOWN, Actions.LEFT, Actions.RIGHT);
    }
}
