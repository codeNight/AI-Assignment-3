package model.actions;

import model.state.State;
import model.state.StochasticActionOutput;
import utils.Constants;

import java.util.Arrays;
import java.util.List;

public class Actions {
    public static final StochasticAction UP = new Up();
    public static final StochasticAction DOWN = new Down();
    public static final StochasticAction LEFT = new Left();
    public static final StochasticAction RIGHT = new Right();

    private static State executeNativeAction(State currentState, int xOffset, int yOffset) {
        int nxtX = currentState.x + xOffset;
        int nxtY = currentState.y + yOffset;

        //  hits a wall
        if (nxtY < 0 || nxtY >= Constants.GRID_HEIGHT || nxtX < 0 || nxtX >= Constants.GRID_WIDTH) {
            return currentState;
        }

        return State.of(nxtX, nxtY);
    }

    private static State executeNativeUp(State currentState) {
        return executeNativeAction(currentState, 0, 1);
    }

    private static State executeNativeDown(State currentState) {
        return executeNativeAction(currentState, 0, -1);
    }

    private static State executeNativeLeft(State currentState) {
        return executeNativeAction(currentState, -1, 0);
    }

    private static State executeNativeRight(State currentState) {
        return executeNativeAction(currentState, 1, 0);
    }

    private static class Up implements StochasticAction {
        @Override
        public List<StochasticActionOutput> accept(State current) throws IllegalArgumentException {
            return Arrays.asList(
                    new StochasticActionOutput(executeNativeUp(current), 0.8),
                    new StochasticActionOutput(executeNativeLeft(current), 0.1),
                    new StochasticActionOutput(executeNativeRight(current), 0.1)
            );
        }
    }

    private static class Down implements StochasticAction {
        @Override
        public List<StochasticActionOutput> accept(State current) throws IllegalArgumentException {
            return Arrays.asList(
                    new StochasticActionOutput(executeNativeDown(current), 0.8),
                    new StochasticActionOutput(executeNativeLeft(current), 0.1),
                    new StochasticActionOutput(executeNativeRight(current), 0.1)
            );
        }
    }

    private static class Left implements StochasticAction {
        @Override
        public List<StochasticActionOutput> accept(State current) throws IllegalArgumentException {
            return Arrays.asList(
                    new StochasticActionOutput(executeNativeLeft(current), 0.8),
                    new StochasticActionOutput(executeNativeUp(current), 0.1),
                    new StochasticActionOutput(executeNativeDown(current), 0.1)
            );
        }
    }

    private static class Right implements StochasticAction {
        @Override
        public List<StochasticActionOutput> accept(State current) throws IllegalArgumentException {
            return Arrays.asList(
                    new StochasticActionOutput(executeNativeRight(current), 0.8),
                    new StochasticActionOutput(executeNativeUp(current), 0.1),
                    new StochasticActionOutput(executeNativeDown(current), 0.1)
            );
        }
    }
}
