package model.state;

import model.actions.StochasticAction;
import model.state.State;

/**
 * Represents a nondeterministic outcome from evaluating an {@link StochasticAction} .
 */
public class StochasticActionOutput {
    private final State outcome;
    private final double outcomeProbability;

    public StochasticActionOutput(State outcome, double outcomeProbability) {
        this.outcome = outcome;
        this.outcomeProbability = outcomeProbability;
    }

    public State getOutcome() {
        return outcome;
    }

    public double getOutcomeProbability() {
        return outcomeProbability;
    }
}
