package model.state;

import model.state.State;

public class RewardsGrid {
    public final double[][] rewardsMap;

    public RewardsGrid(double[][] rewardsMap) {
        this.rewardsMap = rewardsMap;
    }

    public double getReward(State state) {
        return rewardsMap[state.x][state.y];
    }
}
