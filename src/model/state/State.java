package model.state;

public class State {
    public final int x, y;

    public State(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static State of(int x, int y) {
        return new State(x, y);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof State) {
            State other = (State) obj;
            return this.x == other.x && this.y == other.y;
        }
        return false;
    }
}
