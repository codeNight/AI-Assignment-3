package algorithms;

import model.actions.ActionsController;
import model.actions.StochasticAction;
import model.state.RewardsGrid;
import model.state.State;
import model.state.StochasticActionOutput;
import utils.Constants;

import java.util.Arrays;
import java.util.List;

public class ValueIteration {
    private static final double EPSILON =  1e-4;

    private final ActionsController actionsController;

    private Double[][] currUtilities ;
    private Double[][] nextUtilities ;
    private StochasticAction[][] actions = new StochasticAction[Constants.GRID_WIDTH][Constants.GRID_HEIGHT];

    private boolean hasConverged = false;

    public ValueIteration(ActionsController actionsController) {
        this.actionsController = actionsController;
        currUtilities = new Double[Constants.GRID_WIDTH][Constants.GRID_HEIGHT];
        nextUtilities = new Double[Constants.GRID_WIDTH][Constants.GRID_HEIGHT];
        fill2dArray(currUtilities, -1.0);
        fill2dArray(nextUtilities, -1.0);
        currUtilities[2][2] = nextUtilities[2][2] = 10.0;
        currUtilities[0][2] = nextUtilities[0][2] = Constants.r * 1.0;


    }

    public Double[][] getUtilities(List<State> stateSpace, RewardsGrid rewardsGrid, double discountFactor) {
        int loops = 1;
        double errorThreshold = (EPSILON) * (1.0 - discountFactor) / discountFactor;
        for (int i = 0; i < 10; i++){

            double error = -1;

            currUtilities = nextUtilities;

            nextUtilities = new Double[Constants.GRID_WIDTH][Constants.GRID_HEIGHT];

            for (State state : stateSpace) {
                if(state.x == 1 && state.y == 2) {
                    System.out.println("In tricky state");
                }

                double maxUtility = -10e30;
                for(StochasticAction action : actionsController.getLegalActions(state)) {
                    double nextUtil = 0;
                    for(StochasticActionOutput output : action.accept(state)) {
                        State nextState = output.getOutcome();
                        double probabilityOfNextState = output.getOutcomeProbability();
                        nextUtil += probabilityOfNextState * currUtilities[nextState.x][nextState.y];
                    }
                    if(nextUtil > maxUtility) {
                        maxUtility = nextUtil;
                        actions[state.x][state.y] = action;
                    }
                }

                nextUtilities[state.x][state.y] = rewardsGrid.getReward(state) + discountFactor *  maxUtility;
                if(Math.abs(nextUtilities[state.x][state.y] - currUtilities[state.x][state.y]) > error) {
                    error = Math.abs(nextUtilities[state.x][state.y] - currUtilities[state.x][state.y]);
                }
            }
            hasConverged = error < errorThreshold;
        }

        for (int i = 0; i < actions.length; i++) {
            for (int j = 0; j < actions[i].length; j++) {
                System.out.println(i + " " + j + actions[i][j]);
            }
        }
        return currUtilities;
    }
    private boolean isGoalState(State curr) {
        return curr.x == 2 && curr.y ==2;
    }

    private <T> void fill2dArray(T[][] arr, T val) {
        for(T[] row : arr) {
            Arrays.fill(row, val);
        }
    }
}
