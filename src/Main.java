import algorithms.ValueIteration;
import model.state.RewardsGrid;
import model.state.State;
import model.actions.DefaultActionController;
import utils.Constants;
import utils.RewardsGridGenerator;
import utils.StateSpaceGenerator;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<State> stateSpace = StateSpaceGenerator.getStateSpace();
        RewardsGrid rewardsGrid = RewardsGridGenerator.getRewardsGrid(stateSpace);

        for (int i = 0; i < rewardsGrid.rewardsMap.length; i++) {
            for (int j = 0; j < rewardsGrid.rewardsMap[0].length; j++) {
                System.out.print(rewardsGrid.rewardsMap[i][j] + " ");
            }
            System.out.println();
        }

            Double[][] optimalUtility = new ValueIteration(new DefaultActionController()).getUtilities(
                stateSpace, rewardsGrid, 0.99
        );

        for(int i=0 ; i< Constants.GRID_WIDTH ; i++) {
            for (int j = 0 ; j<Constants.GRID_HEIGHT ; j++) {
                System.out.print(optimalUtility[i][j] + " ");
            }
            System.out.println();
        }
    }
}
