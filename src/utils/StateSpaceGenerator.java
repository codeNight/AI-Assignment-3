package utils;

import model.state.State;

import java.util.ArrayList;
import java.util.List;

public class StateSpaceGenerator {
    public static List<State> getStateSpace() {
        List<State> stateSpace = new ArrayList<>();
        for(int i=0 ; i<Constants.GRID_WIDTH ; i++) {
            for (int j=0 ; j<Constants.GRID_HEIGHT ; j++) {
                stateSpace.add(State.of(i, j));
            }
        }
        return stateSpace;
    }
}
