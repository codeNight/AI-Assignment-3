package utils;

import model.state.RewardsGrid;
import model.state.State;

import java.util.List;

public class RewardsGridGenerator {
    private static State variableRewardState = State.of(0, 2);
    private static State terminalState = State.of(2, 2);

    public static RewardsGrid getRewardsGrid(List<State> stateSpace) {
        double[][] rewardsMap = new double[Constants.GRID_WIDTH][Constants.GRID_HEIGHT];
        for(int i=0 ; i<Constants.GRID_WIDTH ; i++){
            for(int j=0 ; j<Constants.GRID_HEIGHT ; j++) {
                State state = State.of(i, j);
                    if (state.equals(variableRewardState)) {
                        rewardsMap[i][j] = Constants.r * 1.0;
                    } else if (state.equals(terminalState)) {
                        rewardsMap[i][j] = 10.0;
                    } else {
                        rewardsMap[i][j] = -1.0;
                    }

            }}

        return new RewardsGrid(rewardsMap);
    }
}
